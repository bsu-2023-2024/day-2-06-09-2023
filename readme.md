# Домашнее задание на 13.09.2023

- [Теория](/%D0%A1%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D1%8B_%D1%81%D1%87%D0%B8%D1%81%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F.pdf)
- [Основы систем счисления](https://habr.com/ru/post/124395/)
- [Системы счисления](https://ru.wikibooks.org/wiki/%D0%A1%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D1%8B_%D1%81%D1%87%D0%B8%D1%81%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F)

- Практика
    - [Задание 1](/task-1.md)
    - [Задание 2](/task-2.md)
    - [Задание 3](/task-3.md)

    Все задания выполняются письменно - вариант согласно порядковом номеру в [списке группы](https://docs.google.com/spreadsheets/d/1MAp3meGfvVCROYvMo_6GvWg6vNYRpur4_GLqEC3joWU/edit#gid=0) (итого у каждого будет 6 задач).
