// 11.09.2023.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

using namespace std;

int main()
{
    int x = 1, y; // x was initialised
    int z = (x + 1 / y + sin(x));
}

/*
     КОНСПЕКТ ПО ЛЕКЦИИ
   Спецификаторы: int (usually 2 bytes), long (usually 4 bytes), short (usually 4 bytes)    short<=int<=long для целочисленных
                  float (4 bytes), double (8 bytes) для чисел с плавающей точкой
                  Если число с точкой (1.0), то оно по умолчанию double 
                  
   Модификаторы: long can be the modificator, unsigned, const


   Операнд: то, над чем происходит действие (операция) 
   Операция: унарные, бинарные (все, кроме присваивания левоассоциативные), тернарные (присваивание, сложение, умножение и т.д.)
             лево и право ассоциативные (слева направо или справо на лево)

   Константы и именованые константы
   Пример синтаксиса:
    const double pi = 3.14;

   Нотации именования сущностей (name convensions):
        pascal case: CountOfApples - yes castom types or methods
        snake case: count_of_apples no
        upper case: PI no
        camel case: countOfApples yes yes yes 

   !!Сайт metanit.com

   !!epam learn.epam.com (курс по git), rsschool (for js)

   Дз: познакомиться с типами int, long, short, char, float, double
*/
